class Contact < ActiveRecord::Base
  attr_accessible :email, :name, :user_id

  belongs_to(
  :user,
  class_name: "User",
  foreign_key: :user_id,
  primary_key: :id
  )

  has_many(
  :contact_groups,
  class_name: "ContactGroup",
  foreign_key: :contact_id,
  primary_key: :id
  )

  has_many(
  :comments,
  class_name: "Comment",
  foreign_key: :contact_id,
  primary_key: :id
  )

end
