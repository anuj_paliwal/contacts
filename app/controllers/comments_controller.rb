class CommentsController < ApplicationController
  def index
    user = User.find_by_id(params[:user_id])
    comments = user.comments
    render :json => comments
  end

  def show
    user = User.find_by_id(params[:user_id])
    comment = Comment.find_by_user_id_and_id(user.id,params[:id])
    render :json => comment
  end

  def update
    user = User.find_by_id(params[:user_id])
    comment = Comment.find_by_user_id_and_id(user.id,params[:id])
    comment.update_attributes(params[:comment])
    if comment.save
      render :json => comment
    else
      render :json => comment.errors, :status => :unprocessable_entity
    end
  end

  def destroy
    user = User.find_by_id(params[:user_id])
    comment = Comment.find_by_user_id_and_id(user.id,params[:id])
    comment.destroy
    head status: :ok
  end

  def create
    comment = Comment.new(params[:comment])
    if comment.save
      render :json => comment
    else
      render :json => comment.errors, :status => :unprocessable_entity
    end
  end

end
