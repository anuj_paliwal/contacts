class UsersController < ApplicationController
  def index
    users = User.all
    render :json => users
  end

  def favorites
    contacts = Contact.find_all_by_user_id_and_favorite(params[:id],true)
    render :json => contacts
  end

  def create
    user = User.new(params[:user])
    if user.save
      render :json => user
    else
      render :json => user.errors, :status => :unprocessable_entity
    end
  end

  def show
    user = User.find_by_id(params[:id])
    render :json => user
  end

  def update
    user = User.find_by_id(params[:id])
    user.update_attributes(params[:user])
    if user.save
      render :json => user
    else
      render :json => user.errors, :status => :unprocessable_entity
    end
  end

  def destroy
    user = User.find_by_id(params[:id])
    user.destroy
    render :text => "User destroyed!"
  end
end
