class GroupsController < ApplicationController

  def index
    groups = Group.all
    render :json => groups
  end

  def create
    group = Group.new(params[:group])
    if group.save
      render :json => group
    else
      render :json => group.errors, :status => :unprocessable_entity
    end
  end

  def show
    group = Group.find_by_id(params[:id])
    render :json => group
  end

  def destroy
    group = Group.find_by_id(params[:id])
    group.destroy
    head :status => :ok
  end

  def update
    group = Group.find_by_id(params[:id])
    group.update_attributes(params[:group])
  end

  def funtimes
    group = Group.find_by_id(params[:id])
    contacts = group.contacts
    render :json => contacts
  end

end
