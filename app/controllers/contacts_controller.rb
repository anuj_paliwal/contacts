class ContactsController < ApplicationController
  def index
    user = User.find_by_id(params[:user_id])
    contacts = []
    contacts += user.contacts
    contacts += user.distant_contacts
    render :json => contacts
  end

  def create
    contact = Contact.new(params[:contact])
    if contact.save
      render :json => contact
    else
      render :json => contact.errors, :status => :unprocessable_entity
    end
  end

  def show
    contact = Contact.find_by_id(params[:id])
    render :json => contact
  end

  def destroy
    contact = Contact.find_by_id(params[:id])
    contact.destroy
    head :status => :ok
  end

  def update
    contact = Contact.find_by_id(params[:id])
    contact.update_attributes(params[:contact])
    if contact.save
      render :json => contact
    else
      render :json => contact.errors, :status => :unprocessable_entity
    end
  end
end
