Contacts::Application.routes.draw do
  resources :users do
    member do
      get 'favorites'
    end
    resources :comments
    resources :contacts, only: :index
  end
  resources :contacts, except: [:new, :edit, :index]
  resources :contact_shares, only: [:create, :destroy]
  resources :groups do
    member do
      get 'funtimes'
    end
  end

end
